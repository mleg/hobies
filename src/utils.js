export const KEY_CODES = {
	enter: 13,
}

export function generateId() {
	return Math.random()
		.toString(36)
		.slice(-8)
}

export function removeNodes(nodeArray) {
	while (nodeArray.length) {
		const node = nodeArray.shift()
		node.remove()
	}
}

export function copy(object) {
	return JSON.parse(JSON.stringify(object))
}
