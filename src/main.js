import 'normalize.css/normalize.css'
import './styles/index.scss'
import { HobbiesController } from './controller'
import { Hobbies } from './model'

const mountPoint = document.getElementById('app')

const modelMine = new Hobbies()
new HobbiesController({ mountPoint, whose: 'mine', model: modelMine })

const modelFriend = new Hobbies(modelMine)
new HobbiesController({ mountPoint, whose: 'friend', model: modelFriend })
