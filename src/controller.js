import 'normalize.css/normalize.css'
import './styles/index.scss'
import * as templates from './templates'
import { KEY_CODES } from './utils'

const initialData = require('@/initial-data.json')

export class HobbiesController {
	whose
	model
	rootNode

	constructor(options) {
		const { whose, model, mountPoint } = options
		this.whose = whose
		this.model = model

		this.model.once('load', this.render(mountPoint))
		this.model.on('newHobby', this.renderHobbyItem)

		this.model.init(initialData[whose])
	}

	render = mountPoint => () => {
		this.rootNode = templates.render('person', this.model)
		mountPoint.appendChild(this.rootNode)

		if (this.whose === 'mine') {
			this.renderInput()
		}

		this.model.hobbies.forEach(this.renderHobbyItem)
	}

	renderInput() {
		const inputContainer = this.rootNode.querySelector('.input-container')
		inputContainer.classList.remove('hidden')

		const onEnter = input => {
			if (!input.value) {
				return
			}
			this.model.addNewHobby(input.value.trim())
			input.value = ''
		}

		const input = inputContainer.querySelector('input')
		input.addEventListener('keyup', event => {
			if (event.keyCode === KEY_CODES.enter) {
				onEnter(event.target)
			}
		})

		input.focus()
	}

	renderHobbyItem = hobby => {
		const parentNode = this.rootNode.querySelector('ul')
		const itemNode = templates.render('hobby-item', hobby)
		parentNode.appendChild(itemNode)

		if (this.whose !== 'mine') {
			itemNode.classList.remove('hobby-item--mine')
		}

		const iconAdd = itemNode.querySelector('.hobby-item__icon-add')
		iconAdd.addEventListener('click', () => {
			this.model.addToConnected(hobby)
		})

		const iconRemove = itemNode.querySelector('.hobby-item__icon-remove')
		iconRemove.addEventListener('click', () => {
			this.model.removeHobby(hobby)
		})

		const onRemove = removedId => {
			if (hobby.id === removedId) {
				this.model.off('removed', onRemove)
				itemNode.remove()
			}
		}
		this.model.on('removed', onRemove)
	}
}
