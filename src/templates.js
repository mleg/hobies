import { removeNodes } from './utils'

const templates = getTemplates()

function getTemplates() {
	const nodes = getTemplateNodes()
	const result = collectTemplates(nodes)
	removeNodes(nodes)

	return result
}

function getTemplateNodes() {
	return Array.from(document.querySelectorAll('template'))
}

function collectTemplates(templateNodes) {
	return templateNodes.reduce((result, tmpl) => {
		const id = tmpl.id.replace(/^tmpl-/, '')
		return {
			...result,
			[id]: tmpl.innerHTML,
		}
	}, {})
}

function insertValues(template, values) {
	const replacer = (match, p1) => {
		return values[p1]
	}
	return template.replace(/{{\s*([^\s]+)\s*}}/g, replacer)
}

export function render(templateName, data) {
	const tempTemplate = document.createElement('template')
	const html = insertValues(templates[templateName], data)
	tempTemplate.innerHTML = html
	return tempTemplate.content.firstElementChild
}
