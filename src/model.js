import EventEmitter from 'eventemitter3'
import { generateId, copy } from './utils'

const addId = object => ({
	...object,
	id: generateId(),
})

export class Hobbies extends EventEmitter {
	id
	title
	hobbies

	connectedModel

	constructor(connectedModel) {
		super()
		this.connectedModel = connectedModel
	}

	init(data) {
		this.id = generateId()
		this.title = data.title
		this.hobbies = copy(data.hobbies).map(addId)
		this.emit('load')
	}

	addNewHobby(text) {
		const hobby = addId({ text })
		this.hobbies.push(hobby)
		this.emit('newHobby', hobby)
	}

	addHobby(hobby) {
		if (this.hobbies.find(item => item.id === hobby.id)) {
			return
		}
		this.hobbies.push(hobby)
		this.emit('newHobby', hobby)
	}

	addToConnected(hobby) {
		if (this.connectedModel) {
			this.connectedModel.addHobby(copy(hobby))
		}
	}

	removeHobby(hobby) {
		const index = this.hobbies.findIndex(item => item.id === hobby.id)
		if (index === -1) {
			return
		}
		this.hobbies.splice(index, 1)
		this.emit('removed', hobby.id)
	}
}
