# Решение тестового задания

## Примечания к решению

Фактичсеки это vanilla js. Рантайм-зависимости минимальны: только готовая имплементация `EventEmitter`, чтобы свою не писать, и `normalize.css`. Размер сборки едва больше 10Kb (в gzip).

Сделал далеко не всё, потому что задание и так очень объёмное: без фреймворков на повседневной основе не пишу -- пришлось потратить много времени на архитектуру.

Использовал `vue-cli` как систему сборки, чтобы самому webpack не настраивать.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
